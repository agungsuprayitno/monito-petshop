# Monito Petshop
Write Monito Petshop from Figma Design using Vue3, Vue Router and Pinia.

### Demo
https://monito.ikanku.my.id/

## Run on local environment

- Clone this repository
   
    ```
    git clone https://gitlab.com/agungsuprayitno/monito-petshop.git
    ``` 

- Install dependencies

    ```
    npm i
    ```


- Run Development server

    ```
    npm run dev
    ```